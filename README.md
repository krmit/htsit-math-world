# HTSIT math World


Alternativa titlar som jag funderat på:


*Math games for noobs from HTSIT*


Spelet är ett skämtsamt sätt att uppmärksamma de dåliga matematikkunskaper som vissa elever på HTS it-inriktning har.


## Spelgång


Efter absurt intro kommer du till HTSIT math World. Du är en lite spelfigur med uppgift att rädda kajsa ifrån den elaka och grymma skolinspektionen.


Din figur startar vid ett hus och kan gå runt i världen. Vissa områden är dock låsta, för att komma till dem måste du klara utmaningar och få föremål. Dessa utmaningar får du genom att gå till ett hus.


Utmaningarna är ett minispel som kräver matematiska kunskaper.


Efter mycket kämpande så vinner du spelet genom att rädda Kajsa!


## Kommentarer


Jag måste se till att “Cutscenes" bara är synliga för andra htsit:are.
