var worldState = { 
    map: null,
    layer: null,
    cursors: null,
    sprite: null,
    create: function () {

        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.map = game.add.tilemap("desert");

        this.map.addTilesetImage("Desert", "tiles");
        
        this.map.setCollisionBetween(1, 3);
        this.map.setCollisionBetween(9, 11);
        this.map.setCollisionBetween(17, 19);

        this.layer = this.map.createLayer("Ground");

        this.layer.resizeWorld();

        this.sprite = game.add.sprite(450, 80, "car");
        this.sprite.anchor.setTo(0.5, 0.5);

        game.physics.enable(this.sprite);

        game.camera.follow(this.sprite);

        this.cursors = game.input.keyboard.createCursorKeys();
    },
    update: function () {
        game.physics.arcade.collide(this.sprite, this.layer);
        this.sprite.body.velocity.x = 0;
        this.sprite.body.velocity.y = 0;
        this.sprite.body.angularVelocity = 0;

        if (this.cursors.left.isDown)
    {
            this.sprite.body.angularVelocity = -200;
        }
        else if (this.cursors.right.isDown)
    {
            this.sprite.body.angularVelocity = 200;
        }

        if (this.cursors.up.isDown)
    {
            this.sprite.body.velocity.copyFrom(game.physics.arcade.velocityFromAngle(this.sprite.angle, 300));
        }

    },

    render: function() {
        game.debug.text("Click to fill tiles", 32, 32, "rgb(0,0,0)");
        game.debug.text("Tile X: " + this.layer.getTileX(this.sprite.x), 32, 48, "rgb(0,0,0)");
        game.debug.text("Tile Y: " + this.layer.getTileY(this.sprite.y), 32, 64, "rgb(0,0,0)");
    }
};
