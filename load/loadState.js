var loadState = {
                preload: function() {
                    var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
                    loadLabel = game.add.text(80,150, "Loading ...", style);
                    game.load.image("skyBackground","assets/skyBackground.png");
                    game.load.tilemap('desert', 'assets/maps/desert.json', null, Phaser.Tilemap.TILED_JSON);
                    game.load.image('tiles', 'assets/maps/desert.png');
                    game.load.image('car', 'assets/character/car.png');
                },

                create: function() {
                    game.state.start('intro');
                }
};
